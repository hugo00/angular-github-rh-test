﻿using System;
using System.Collections.Generic;
using ExerciciosLinq.Business;
using ExerciciosLinq.Business.Exercicios;
using Microsoft.Extensions.DependencyInjection;

namespace ExerciciosLinq
{
	internal class Program
	{
		private static int Main(string[] args)
		{
			if (args.Length != 1 || (args[0] != "1" && args[0] != "2" && args[0] != "3"))
			{
				Console.WriteLine("Necessário informar o número do exercício que deseja executar (1, 2 ou 3). Ex: ExercicioLinq.exe 1");
				return 1;
			}

			var serviceProvider = ConfigureServices();

			var obterExercicio = serviceProvider.GetService<Func<string, IExercicio>>();
			var exercicio = obterExercicio(args[0]);
			exercicio.Executar();
			return 0;
		}

		private static ServiceProvider ConfigureServices()
		{
			var serviceCollection = new ServiceCollection();
			serviceCollection.AddTransient<IPrint, PrintHelper>();

			serviceCollection.AddTransient<ProblemaCollatz>();
			serviceCollection.AddTransient<Exercicio1>();
			serviceCollection.AddTransient<Exercicio2>();
			serviceCollection.AddTransient<Exercicio3>();

			serviceCollection.AddTransient(factory =>
			{
				IExercicio ObterExercicio(string key)
				{
					switch (key)
					{
						case "1":
							return factory.GetService<Exercicio1>();
						case "2":
							return factory.GetService<Exercicio2>();
						case "3":
							return factory.GetService<Exercicio3>();
						default:
							throw new KeyNotFoundException();
					}
				}

				return (Func<string, IExercicio>) ObterExercicio;
			});

			return serviceCollection.BuildServiceProvider();
		}
	}
}
