using ExerciciosLinq.Business.Exercicios;
using Xunit;

namespace ExerciciosLinq.Business.Tests
{
	public class Exercicio3Tests : BaseTest
	{
		[Fact]
		public void Imprimir_numeros_do_primeiro_array_que_nao_estejam_contidos_no_segundo()
		{
			new Exercicio3(Print).Executar();
			Assert.Equal(new[] { "{ 1, 29, 42, 98, 234 }" }, MensagensImprimidas);
		}
	}
}
