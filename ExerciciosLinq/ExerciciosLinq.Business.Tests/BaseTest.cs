﻿using System.Collections.Generic;
using NSubstitute;

namespace ExerciciosLinq.Business.Tests
{
	public abstract class BaseTest
	{
		protected List<string> MensagensImprimidas;
		protected IPrint Print;

		protected BaseTest()
		{
			MockPrint();
		}

		private void MockPrint()
		{
			MensagensImprimidas = new List<string>();
			Print = Substitute.For<IPrint>();
			Print.Log(Arg.Do<string>(m => MensagensImprimidas.Add(m)));
		}
	}
}