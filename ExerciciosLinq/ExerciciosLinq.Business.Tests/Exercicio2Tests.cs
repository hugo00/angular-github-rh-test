using ExerciciosLinq.Business.Exercicios;
using Xunit;

namespace ExerciciosLinq.Business.Tests
{
    public class Exercicio2Tests : BaseTest
    {
        [Fact]
        public void Verifica_se_array_contem_apenas_numeros_impares()
        {
			new Exercicio2(Print).Executar();
			Assert.Equal(new[] { "{ 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 } - n�o cont�m apenas n�meros �mpares" }, MensagensImprimidas);

	        var possuiApenasImpares = Exercicio2.Numeros.PossuiApenasImpares();
			Assert.False(possuiApenasImpares);

	        possuiApenasImpares = new[] { 1, 3, 5, 7, 9, 11, 33 }.PossuiApenasImpares();
			Assert.True(possuiApenasImpares);

	        possuiApenasImpares = new[] { 1, 3, 5, 7, 9, 11, 32 }.PossuiApenasImpares();
			Assert.False(possuiApenasImpares);
        }
    }
}
