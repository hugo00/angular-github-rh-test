﻿using System;

namespace ExerciciosLinq.Business
{
	public class PrintHelper : IPrint
	{
		public void Log(string message)
		{
			Console.WriteLine(message);
		}
	}
}