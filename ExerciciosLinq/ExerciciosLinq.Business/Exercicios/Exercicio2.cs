﻿using System;

namespace ExerciciosLinq.Business.Exercicios
{
	public class Exercicio2 : IExercicio
	{
		internal static readonly int[] Numeros = { 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 };
		private readonly IPrint _print;

		public Exercicio2(IPrint print)
		{
			_print = print;
		}

		public void Executar()
		{
			var possuiApenasImpares = Numeros.PossuiApenasImpares();
			var texto = possuiApenasImpares ? string.Empty : "não ";
			_print.Log($"{Numeros.FormatarParaImpressao()} - {texto}contém apenas números ímpares");
		}
	}
}