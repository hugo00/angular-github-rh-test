﻿using System.Linq;

namespace ExerciciosLinq.Business.Exercicios
{
	public class Exercicio3 : IExercicio
	{
		private readonly IPrint _print;
		internal static readonly int[] PrimeiroArray = { 1, 3, 7, 29, 42, 98, 234, 93 };
		internal static readonly int[] SegundoArray = { 4, 6, 93, 7, 55, 32, 3 };

		public Exercicio3(IPrint print)
		{
			_print = print;
		}

		public void Executar()
		{
			var resultado = PrimeiroArray.Except(SegundoArray);
			_print.Log($"{resultado.FormatarParaImpressao()}");
		}
	}
}