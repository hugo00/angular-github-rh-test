﻿using System.Collections.Generic;
using System.Linq;

namespace ExerciciosLinq.Business.Exercicios
{
	public class Exercicio1 : IExercicio
	{
		private readonly IPrint _print;
		private readonly ProblemaCollatz _problemaCollatz;

		public Exercicio1(IPrint print, ProblemaCollatz problemaCollatz)
		{
			_print = print;
			_problemaCollatz = problemaCollatz;
		}

		public void Executar()
		{
			const int min = 1;
			const int max = 1000000;

			IEnumerable<int> maiorSequencia = null;
			var tamanhoMaiorSequencia = -1;
			var valorInicialParaMaiorSequencia = -1;
			for (int i = min; i <= max; i++)
			{
				var resultado = _problemaCollatz.ObterSequencia(i);

				var tamanhoSequencia = resultado.Count();
				if (tamanhoSequencia > tamanhoMaiorSequencia)
				{
					tamanhoMaiorSequencia = tamanhoSequencia;
					maiorSequencia = resultado;
					valorInicialParaMaiorSequencia = i;
				}
			}

			_print.Log($"Valor inicial para maior sequencia({min}-{max}): {valorInicialParaMaiorSequencia}");
			_print.Log($"Maior tamanho sequencia: {tamanhoMaiorSequencia}");
			_print.Log($"{maiorSequencia.FormatarParaImpressao()}");
		}
	}
}