﻿namespace ExerciciosLinq.Business.Exercicios
{
	public interface IExercicio
	{
		void Executar();
	}
}