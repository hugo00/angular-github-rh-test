﻿using System.Collections.Generic;
using System.Linq;

namespace ExerciciosLinq.Business
{
    public static class IEnumerableExtensions
    {
	    public static bool PossuiApenasImpares(this IEnumerable<int> lista)
	    {
		    return lista.All(m => m % 2 != 0);
	    }

	    public static string FormatarParaImpressao(this IEnumerable<int> lista)
	    {
		    return $"{{ {string.Join(", ", lista)} }}";
	    }
    }
}
