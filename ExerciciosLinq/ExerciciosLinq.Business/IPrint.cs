﻿namespace ExerciciosLinq.Business
{
	public interface IPrint
	{
		void Log(string message);
	}
}