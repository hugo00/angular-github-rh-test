﻿using System.Collections.Generic;

namespace ExerciciosLinq.Business
{
	public class ProblemaCollatz
	{
		public IEnumerable<int> ObterSequencia(int valorInicial)
		{
			var sequenciaInicial = new List<int> { valorInicial };
			return ObterSequencia(sequenciaInicial, valorInicial);
		}

		private List<int> ObterSequencia(List<int> sequenciaAtual, int valorAtual)
		{
			if (valorAtual <= 1) return sequenciaAtual;

			int proximoValor;
			if (valorAtual % 2 == 0)
				proximoValor =  valorAtual / 2;
			else
				proximoValor = 3 * valorAtual + 1;

			sequenciaAtual.Add(proximoValor);
			return ObterSequencia(sequenciaAtual, proximoValor);
		}
	}
}