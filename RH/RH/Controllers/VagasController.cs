﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RH.Data;
using RH.Models;

namespace RH.Controllers
{
    public class VagasController : BaseController<Vaga>
    {
	    private readonly DomainContext _context;

	    public VagasController(DomainContext context) : base(context)
	    {
		    _context = context;
	    }

		[HttpPost("atualizarTecnologia")]
		public void AtualizarTecnologia([FromBody]VagaTecnologia vagaTecnologia)
		{
			var vagaTecnologiaEncontrada = _context.VagaTecnologia
				.Where(m => m.VagaId == vagaTecnologia.VagaId)
				.FirstOrDefault(m => m.TecnologiaId == vagaTecnologia.TecnologiaId);
			if (vagaTecnologiaEncontrada != null)
				_context.Remove(vagaTecnologiaEncontrada);
			else
				_context.Add(vagaTecnologia);
			_context.SaveChanges();
		}

		[HttpPost("atualizarCandidato")]
		public void AtualizarCandidato([FromBody]VagaCandidato vagaCandidato)
		{
			var vagaCandidatoEncontrado = _context.VagaCandidato
				.Where(m => m.VagaId == vagaCandidato.VagaId)
				.FirstOrDefault(m => m.CandidatoId == vagaCandidato.CandidatoId);
			if (vagaCandidatoEncontrado != null)
				_context.Remove(vagaCandidatoEncontrado);
			else
				_context.Add(vagaCandidato);
			_context.SaveChanges();
		}

		[HttpGet("candidatosPossiveisParaVaga/{id}")]
		public IEnumerable<Candidato> CandidatosPossiveisParaVaga(int id)
		{
			var entities = _context.Entities<Candidato>();
			return entities.Where(m => m.Vagas.All(v => v.VagaId != id))
				.ToList();
		}

	    protected override IQueryable<Vaga> IncludesAdditionalInformation(DbSet<Vaga> entities)
	    {
		    return entities
			    .Include(m => m.Candidatos)
					.ThenInclude(m => m.Candidato)
					.ThenInclude(m => m.Tecnologias)
			    .Include(m => m.Tecnologias);
	    }
    }
}
