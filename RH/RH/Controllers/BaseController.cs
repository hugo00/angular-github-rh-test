﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RH.Data;
using RH.Models;

namespace RH.Controllers
{
    [Route("api/[controller]")]
	public abstract class BaseController<TEntity> : Controller
	    where TEntity : class, IEntity
    {
		private readonly DomainContext _context;

		protected BaseController(DomainContext context)
		{
			_context = context;
		}

		[HttpGet]
		public IEnumerable<TEntity> Get()
		{
			return IncludesAdditionalInformation(_context.Entities<TEntity>())
				.ToList();
		}

	    protected virtual IQueryable<TEntity> IncludesAdditionalInformation(DbSet<TEntity> entities)
	    {
		    return entities;
	    }

	    [HttpGet("{id}")]
		public TEntity Get(int id)
		{
			return IncludesAdditionalInformation(_context.Entities<TEntity>()).First(m => m.Id == id);
		}

		[HttpPost]
		public TEntity Post([FromBody]TEntity entity)
		{
			var result = _context.SaveOrUpdate(entity);
			return result;
		}

		[HttpPut("{id}")]
		public void Put(int id, [FromBody]string value)
		{
		}

		[HttpDelete("{id}")]
		public void Delete(int id)
		{
			_context.Remove<TEntity>(id);
		}
	}
}