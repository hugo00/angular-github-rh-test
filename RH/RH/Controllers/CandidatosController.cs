﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RH.Data;
using RH.Models;

namespace RH.Controllers
{
    public class CandidatosController : BaseController<Candidato>
    {
	    private readonly DomainContext _context;

	    public CandidatosController(DomainContext context) : base(context)
	    {
		    _context = context;
	    }

		[HttpPost("atualizarTecnologia")]
		public void AtualizarTecnologia([FromBody]CandidatoTecnologia candidatoTecnologia)
		{
			var candidatoTecnologiaEncontrada = _context.CandidatoTecnologia
				.Where(m => m.CandidatoId == candidatoTecnologia.CandidatoId)
				.FirstOrDefault(m => m.TecnologiaId == candidatoTecnologia.TecnologiaId);
			if (candidatoTecnologiaEncontrada != null)
				_context.Remove(candidatoTecnologiaEncontrada);
			else
				_context.Add(candidatoTecnologia);
			_context.SaveChanges();
		}

	    protected override IQueryable<Candidato> IncludesAdditionalInformation(DbSet<Candidato> entities)
	    {
		    return entities.Include(m => m.Tecnologias);
	    }
    }
}
