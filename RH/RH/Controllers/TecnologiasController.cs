﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using RH.Data;
using RH.Models;

namespace RH.Controllers
{
    public class TecnologiasController : BaseController<Tecnologia>
    {
	    public TecnologiasController(DomainContext context) : base(context)
	    {
	    }

	    protected override IQueryable<Tecnologia> IncludesAdditionalInformation(DbSet<Tecnologia> entities)
	    {
		    return entities.Include(m => m.Candidatos);
	    }
    }
}
