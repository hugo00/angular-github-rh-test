﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace RH.Data
{
	public static class Registrations
	{
		public static void Run(IServiceCollection service, IConfiguration configuration)
		{
	        var connection = configuration["ConexaoSqlite:SqliteConnectionString"];

			AddContext<DomainContext>(service, connection);
		}

		private static void AddContext<TContext>(IServiceCollection service, string connectionString) 
			where TContext : DbContext
		{
	        service.AddDbContext<TContext>(options => options.UseSqlite(connectionString));
		}
	}
}