﻿using System.Linq;
using System.Threading.Tasks;
using RH.Models;

namespace RH.Data.Seeds
{
	public class TecnologiasSeed : ISeed
	{
		private readonly DomainContext _context;

		public TecnologiasSeed(DomainContext context)
		{
			_context = context;
		}

		public async Task Run()
		{
		    if (_context.Entities<Tecnologia>().Any())
			    return;
		    var tecnologia = new Tecnologia { Descricao = "Uhull" };
		    _context.Add(tecnologia);
		    await _context.SaveChangesAsync();
		}
	}
}