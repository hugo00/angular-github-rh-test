﻿using System.Threading.Tasks;

namespace RH.Data.Seeds
{
	public interface ISeed
	{
		Task Run();
	}
}