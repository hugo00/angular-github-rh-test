﻿using System.Linq;
using System.Threading.Tasks;
using RH.Models;

namespace RH.Data.Seeds
{
	public class CandidatosSeed : ISeed
	{
		private readonly DomainContext _context;

		public CandidatosSeed(DomainContext context)
		{
			_context = context;
		}

		public async Task Run()
		{
		    if (_context.Entities<Candidato>().Any())
			    return;
		    var candidato = new Candidato { Nome = "Candidato 1" };
		    _context.Add(candidato);
		    await _context.SaveChangesAsync();
		}
	}
}