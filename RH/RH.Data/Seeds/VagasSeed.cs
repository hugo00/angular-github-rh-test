﻿using System.Linq;
using System.Threading.Tasks;
using RH.Models;

namespace RH.Data.Seeds
{
	public class VagasSeed : ISeed
	{
		private readonly DomainContext _context;

		public VagasSeed(DomainContext context)
		{
			_context = context;
		}

		public async Task Run()
		{
		    if (_context.Entities<Vaga>().Any())
			    return;
		    var vaga = new Vaga { Descricao = "Vago" };
		    _context.Add(vaga);
		    await _context.SaveChangesAsync();
		}
	}
}