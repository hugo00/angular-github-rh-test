﻿using Microsoft.Extensions.DependencyInjection;

namespace RH.Data.Seeds
{
	public static class Registrations
	{
		public static void Run(IServiceCollection service)
		{
			service.AddTransient<ISeed, TecnologiasSeed>();
			service.AddTransient<ISeed, CandidatosSeed>();
			service.AddTransient<ISeed, VagasSeed>();
		}
	}
}