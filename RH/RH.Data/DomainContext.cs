﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RH.Data.Mappings;
using RH.Models;

namespace RH.Data
{
	public class DomainContext : DbContext
	{
		public DomainContext(DbContextOptions<DomainContext> options)
			: base(options)
		{
		}

		public DbSet<Tecnologia> Tecnologias { get; set; }
		public DbSet<Candidato> Candidatos { get; set; }
		public DbSet<Vaga> Vagas { get; set; }
		public DbSet<CandidatoTecnologia> CandidatoTecnologia { get; set; }
		public DbSet<VagaTecnologia> VagaTecnologia { get; set; }
		public DbSet<VagaCandidato> VagaCandidato { get; set; }


		public TEntity SaveOrUpdate<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			var entities = Entities<TEntity>();

			var entidadeResultante = entity.Id != 0 
				? entities.Update(entity).Entity 
				: entities.Add(entity).Entity;

			SaveChanges();
			return entidadeResultante;
		}

		public void Remove<TEntity>(int id)
			where TEntity : class
		{
			var entities = Entities<TEntity>();
			var entity = entities.Find(id);
			entities.Remove(entity);
			SaveChanges();
		}

		public DbSet<TEntity> Entities<TEntity>()
			where TEntity : class
		{
			var property = GetType()
				.GetProperties()
				.Where(m => m.PropertyType.IsGenericType)
				.First(m => m.PropertyType.GetGenericArguments().Contains(typeof(TEntity)));
			var entities = (DbSet<TEntity>)property.GetValue(this);
			return entities;
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			ConfigureMappings(builder);

			base.OnModelCreating(builder);
		}

		private static void ConfigureMappings(ModelBuilder builder)
		{
			var entityMappingInterface = typeof(IEntityMapping);
			var mappings = 
				AppDomain.CurrentDomain.GetAssemblies().SelectMany(m => m.GetTypes())
				.Where(m => !m.IsAbstract)
				.Where(m => !m.IsInterface)
				.Where(m => m.IsClass)
				.Where(m => entityMappingInterface.IsAssignableFrom(m));
			foreach (var mapping in mappings)
			{
				((IEntityMapping)Activator.CreateInstance(mapping)).Map(builder);
			}
		}
	}
}