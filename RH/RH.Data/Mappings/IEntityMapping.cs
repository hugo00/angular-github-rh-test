﻿using Microsoft.EntityFrameworkCore;

namespace RH.Data.Mappings
{
	public interface IEntityMapping
	{
		void Map(ModelBuilder builder);
	}
}