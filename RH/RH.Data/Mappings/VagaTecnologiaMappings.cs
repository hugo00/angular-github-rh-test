﻿using Microsoft.EntityFrameworkCore;
using RH.Models;

namespace RH.Data.Mappings
{
	public class VagaTecnologiaMappings : IEntityMapping
	{
		public void Map(ModelBuilder builder)
		{
			builder.Entity<VagaTecnologia>()
				.HasKey(t => new {t.VagaId, t.TecnologiaId});

			builder.Entity<VagaTecnologia>()
				.HasOne(m => m.Vaga)
				.WithMany(t => t.Tecnologias)
				.HasForeignKey(m => m.VagaId);
			builder.Entity<VagaTecnologia>()
				.HasOne(m => m.Tecnologia)
				.WithMany(t => t.Vagas)
				.HasForeignKey(m => m.TecnologiaId);
		}
	}
}