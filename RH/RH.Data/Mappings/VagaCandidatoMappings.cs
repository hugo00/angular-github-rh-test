﻿using Microsoft.EntityFrameworkCore;
using RH.Models;

namespace RH.Data.Mappings
{
	public class VagaCandidatoMappings : IEntityMapping
	{
		public void Map(ModelBuilder builder)
		{
			builder.Entity<VagaCandidato>()
				.HasKey(t => new {t.VagaId, t.CandidatoId});

			builder.Entity<VagaCandidato>()
				.HasOne(m => m.Vaga)
				.WithMany(t => t.Candidatos)
				.HasForeignKey(m => m.VagaId);
			builder.Entity<VagaCandidato>()
				.HasOne(m => m.Candidato)
				.WithMany(t => t.Vagas)
				.HasForeignKey(m => m.CandidatoId);
		}
	}
}