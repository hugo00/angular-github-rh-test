﻿using Microsoft.EntityFrameworkCore;
using RH.Models;

namespace RH.Data.Mappings
{
	public class CandidatoTecnologiaMappings : IEntityMapping
	{
		public void Map(ModelBuilder builder)
		{
			builder.Entity<CandidatoTecnologia>()
				.HasKey(t => new {t.CandidatoId, t.TecnologiaId});

			builder.Entity<CandidatoTecnologia>()
				.HasOne(m => m.Candidato)
				.WithMany(t => t.Tecnologias)
				.HasForeignKey(m => m.CandidatoId);
			builder.Entity<CandidatoTecnologia>()
				.HasOne(m => m.Tecnologia)
				.WithMany(t => t.Candidatos)
				.HasForeignKey(m => m.TecnologiaId);
		}
	}
}