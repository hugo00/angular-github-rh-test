﻿using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RH.Data.Seeds;

namespace RH.Data
{
	public static class InitializeMigrations
	{
		public static async Task Initialize(IServiceProvider service)
	    {
		    using (var serviceScope = service.CreateScope())
		    {
			    var scopeServiceProvider = serviceScope.ServiceProvider;
				Migrate<DomainContext>(scopeServiceProvider);
			    try
			    {
				    await InsertTestData(scopeServiceProvider);
			    }
			    catch (SqliteException ex)
			    {
				    var loggerFactory = service.GetService<ILoggerFactory>();
				    var logger = loggerFactory.CreateLogger(nameof(InitializeMigrations));
				    logger.LogDebug(ex, ex.Message);
			    }
		    }
	    }

		private static void Migrate<TDbContext>(IServiceProvider scopeServiceProvider)
			where TDbContext : DbContext
		{
			var db = scopeServiceProvider.GetService<TDbContext>();
			db.Database.Migrate();
		}

	    private static async Task InsertTestData(IServiceProvider service)
	    {
			var seeds = service.GetServices<ISeed>();
			foreach (var seed in seeds)
			{
				await seed.Run();
			}
	    }
	}
}