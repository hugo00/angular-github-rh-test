﻿namespace RH.Models
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}