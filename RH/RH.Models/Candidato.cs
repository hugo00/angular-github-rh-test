﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RH.Models
{
	public class Candidato : IEntity
	{
		[Key]
		public int Id { get; set; }
		public string Nome { get; set; }

		public IEnumerable<CandidatoTecnologia> Tecnologias { get; set; }
		public IEnumerable<VagaCandidato> Vagas { get; set; }
	}
}