﻿namespace RH.Models
{
	public class VagaCandidato
	{
		public int VagaId { get; set; }
		public Vaga Vaga { get; set; }

		public int CandidatoId { get; set; }
		public Candidato Candidato { get; set; }
	}
}