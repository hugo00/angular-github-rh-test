﻿namespace RH.Models
{
	public class CandidatoTecnologia
	{
		public int CandidatoId { get; set; }
		public Candidato Candidato { get; set; }

		public int TecnologiaId { get; set; }
		public Tecnologia Tecnologia { get; set; }
	}
}