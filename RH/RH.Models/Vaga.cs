﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RH.Models
{
	public class Vaga : IEntity
	{
		[Key]
		public int Id { get; set; }
		public string Descricao { get; set; }

		public IEnumerable<VagaTecnologia> Tecnologias { get; set; }
		public IEnumerable<VagaCandidato> Candidatos { get; set; }
	}
}