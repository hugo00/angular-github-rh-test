﻿namespace RH.Models
{
	public class VagaTecnologia
	{
		public int VagaId { get; set; }
		public Vaga Vaga { get; set; }

		public int TecnologiaId { get; set; }
		public Tecnologia Tecnologia { get; set; }

		public int Nota { get; set; }
	}
}