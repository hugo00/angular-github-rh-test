﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RH.Models
{
	public class Tecnologia : IEntity
	{
		[Key]
		public int Id { get; set; }
		public string Descricao { get; set; }

		public IEnumerable<CandidatoTecnologia> Candidatos { get; set; }
		public IEnumerable<VagaTecnologia> Vagas { get; set; }
	}
}