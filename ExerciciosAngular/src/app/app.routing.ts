import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './geral/page-not-found/page-not-found.component';
import { AppComponent } from './app.component';
import { UsuariosComponent } from './github/usuarios/usuarios.component';
import { UsuarioComponent } from './github/usuario/usuario.component';
import { MainComponent } from './geral/main/main.component';
import { GithubComponent } from './github/github/github.component';
import { TecnologiasComponent } from './rh/tecnologias/tecnologias.component';
import { RhComponent } from './rh/rh/rh.component';
import { RhLayoutComponent } from './rh/rh-layout/rh-layout.component';
import { CandidatosComponent } from './rh/candidatos/candidatos.component';
import { VagasComponent } from './rh/vagas/vagas.component';
import { CandidatoComponent } from './rh/candidato/candidato.component';
import { VagaComponent } from './rh/vaga/vaga.component';
import { EntrevistaComponent } from './rh/workflows/entrevista/entrevista.component';

export const APP_ROUTING: ModuleWithProviders = RouterModule.forRoot([
    { path: '', component: MainComponent, pathMatch: 'full' },

    {
        path: 'github', component: GithubComponent,
        children: [
            { path: '', redirectTo: 'Usuarios', pathMatch: 'full' },
            { path: 'Usuario/:login', component: UsuarioComponent },
            { path: 'Usuarios', component: UsuariosComponent },
            { path: '**', component: PageNotFoundComponent }
        ]
    },

    {
        path: 'RH', component: RhLayoutComponent,
        children: [
            { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
            { path: 'Dashboard', component: RhComponent },
            { path: 'Tecnologias', component: TecnologiasComponent },
            { path: 'Candidato/:id', component: CandidatoComponent },
            { path: 'Candidatos', component: CandidatosComponent },
            { path: 'Vaga/:id', component: VagaComponent },
            { path: 'Vagas', component: VagasComponent },
            { path: 'Entrevista', component: EntrevistaComponent },
            { path: '**', component: PageNotFoundComponent }
        ]
    },

    { path: '**', component: PageNotFoundComponent }
]);
