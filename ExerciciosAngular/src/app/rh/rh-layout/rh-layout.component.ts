import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rh-layout',
  templateUrl: './rh-layout.component.html',
  styleUrls: ['./rh-layout.component.css']
})
export class RhLayoutComponent implements OnInit {
  links = [
    { display: 'Home', route: '/' },
    { display: 'Dashboard', route: '/RH/Dashboard' },
    { display: 'Tecnologias', route: '/RH/Tecnologias' },
    { display: 'Candidatos', route: '/RH/Candidatos' },
    { display: 'Vagas', route: '/RH/Vagas' },
    { display: 'Entrevista', route: '/RH/Entrevista' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
