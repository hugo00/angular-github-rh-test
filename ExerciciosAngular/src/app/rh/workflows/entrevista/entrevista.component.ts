import { Component, OnInit, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

import { VagasService } from '../../vagas/vagas.service';
import { CandidatosService } from '../../candidatos/candidatos.service';
import { VagaService } from '../../vaga/vaga.service';

@Component({
  selector: 'app-entrevista',
  templateUrl: './entrevista.component.html',
  styleUrls: ['./entrevista.component.css']
})
export class EntrevistaComponent implements OnInit, DoCheck {
  isLinear = true;

  vagas;
  vaga;
  oldVagaValue;

  candidatos;
  candidato;

  finalizado = false;

  constructor(
    private vagasService: VagasService,
    private vagaService: VagaService,
    private candidatosService: CandidatosService,
    private router: Router
  ) { }

  ngOnInit() {
    this.atualizarVagas();
  }

  ngDoCheck() {
    if (this.oldVagaValue !== this.vaga) {
      this.oldVagaValue = this.vaga;
      this.atualizarCandidatos();
    }
  }

  get step1Completed() {
    return this.vaga;
  }
  get step2Completed() {
    return this.candidato;
  }

  atualizarVagas(vagaSelecionada?) {
    this.vagasService.getEntities().subscribe(vagas => {
      this.vagas = vagas;
      if (vagaSelecionada) {
        this.vaga = vagaSelecionada.id;
      }
    });
  }

  atualizarCandidatos(candidatoSelecionado?) {
    this.vagasService.candidatosPossiveisParaVaga(this.vaga)
      .subscribe(candidatos => {
        this.candidatos = candidatos;
        if (candidatoSelecionado) {
          this.candidato = candidatoSelecionado.id;
        } else {
          this.candidato = undefined;
        }
      });
  }

  openAddVagaDialog() {
    this.vagasService.novaVaga(undefined, (vaga) => {
      this.atualizarVagas(vaga);
    });
  }

  possuiVagas() {
    return this.vagas && this.vagas.length > 0;
  }

  openAddCandidatoDialog() {
    this.candidatosService.novoCandidato(undefined, (candidato) => {
      this.atualizarCandidatos(candidato);
    });
  }

  possuiCandidatos() {
    return this.candidatos && this.candidatos.length > 0;
  }

  vincularVagaCandidato() {
    this.finalizado = true;
    this.vagaService.updateCandidatoByIds(this.vaga, this.candidato)
      .subscribe(result => {
        this.router.navigateByUrl('/RH');
      });
  }

}
