import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';
import { RemoveDialogComponent } from '../../geral/dialogs/remove-dialog/remove-dialog.component';
import { VagasService } from './vagas.service';

@Component({
  selector: 'app-vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.css']
})
export class VagasComponent implements OnInit {
  displayedColumns = ['actions', 'vaga'];
  dataSource;

  constructor(
    private vagasService: VagasService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.atualizarLista();
  }

  atualizarLista() {
    this.vagasService.getEntities().subscribe(vagas => {
      this.dataSource = vagas;
    });
  }

  openAddDialog(vaga) {
    this.vagasService.novaVaga(vaga, () => this.atualizarLista());
  }

  openRemoveDialog(vaga) {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      data: {
        title: 'Confirma a deleção da Vaga?',
        body: 'A vaga *' + vaga.descricao + '* será removida',
        descricao: vaga.descricao,
        id: vaga.id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      this.vagasService.removeEntity(result.id).subscribe(_ => {
        this.atualizarLista();
      });
    });
  }

}
