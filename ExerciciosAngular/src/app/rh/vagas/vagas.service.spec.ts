import { TestBed, inject } from '@angular/core/testing';

import { VagasService } from './vagas.service';

describe('VagasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VagasService]
    });
  });

  it('should be created', inject([VagasService], (service: VagasService) => {
    expect(service).toBeTruthy();
  }));
});
