import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';

import { CrudServiceBase } from '../../geral/crud-service-base.service';
import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';

@Injectable()
export class VagasService extends CrudServiceBase {

  protected apiBasePath = 'vagas';

  constructor(
    httpClient: HttpClient,
    public dialog: MatDialog
  ) {
    super(httpClient);
  }

  atualizarTecnologia(tecnologia) {
    return this.http.post<any>(this.endpoint + '/atualizarTecnologia', tecnologia);
  }

  atualizarCandidato(candidato) {
    return this.http.post<any>(this.endpoint + '/atualizarCandidato', candidato);
  }

  candidatosPossiveisParaVaga(vagaId) {
    return this.http.get<any>(this.endpoint + '/candidatosPossiveisParaVaga/' + vagaId);
  }

  novaVaga(vaga, callback?) {
    function runCallback(vagaCriada) {
      if (callback) {
        callback(vagaCriada);
      }
    }

    const data: any = {
      title: 'Informe a descrição da vaga'
    };
    if (vaga) {
      data.id = vaga.id;
      data.descricao = vaga.descricao;
    }
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      if (result.id) {
        this.updateEntity(result).subscribe(savedEntity => runCallback(savedEntity));
      } else {
        this.addEntity(result).subscribe(savedEntity => runCallback(savedEntity));
      }
    });
  }
}
