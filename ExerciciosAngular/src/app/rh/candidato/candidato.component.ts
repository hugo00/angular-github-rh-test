import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';
import { RemoveDialogComponent } from '../../geral/dialogs/remove-dialog/remove-dialog.component';
import { CandidatoService } from './candidato.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidato',
  templateUrl: './candidato.component.html',
  styleUrls: ['./candidato.component.css']
})
export class CandidatoComponent implements OnInit, OnChanges {
  displayedColumns = ['actions', 'tecnologia'];
  tecnologias: any[];

  candidato = {};
  candidatoId;

  @Input()
  candidatoIdInformado;

  @Input()
  hideToolbar;

  constructor(
    private route: ActivatedRoute,
    private candidatoService: CandidatoService,
    public dialog: MatDialog,
    private location: Location
  ) {
    this.route.params.subscribe(params =>
      this.candidatoId = params['id']
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['candidatoIdInformado']) {
      this.candidatoId = this.candidatoIdInformado;
      this.atualizarLista();
    }
  }

  ngOnInit() {
    this.atualizarLista();
  }

  atualizarLista() {
    if (!this.candidatoId) {
      return;
    }

    this.candidatoService.obterInformacoesCandidato(this.candidatoId)
      .subscribe(informacoes => {
        this.candidato = informacoes.candidato;
        this.tecnologias = informacoes.tecnologias;
      });
  }

  updateTecnologia(tecnologia) {
    this.candidatoService.updateTecnologia(this.candidato, tecnologia);
  }

  back() {
    this.location.back();
  }
}
