import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';

import { CandidatosService } from '../candidatos/candidatos.service';
import { TecnologiasService } from '../tecnologias/tecnologias.service';

@Injectable()
export class CandidatoService {

  constructor(
    private candidatosService: CandidatosService,
    private tecnologiasService: TecnologiasService
  ) { }

  obterInformacoesCandidato(id) {
    return Observable.combineLatest(
      this.candidatosService.getEntity(id),
      this.tecnologiasService.getEntities(),
    ).map(informacoes => {
      const [candidato, tecnologias] = informacoes;
      if (candidato.tecnologias && candidato.tecnologias.length > 0) {
        tecnologias.forEach(tecnologia => {
          if (this.candidatoPossuiTecnologia(candidato, tecnologia.id)) {
            tecnologia.possuiTecnologia = true;
          }
        });
      }
      return { candidato, tecnologias };
    });
  }

  updateTecnologia(candidato: any, tecnologia: any): any {
    const novaTecnologia = {
      candidatoId: candidato.id,
      tecnologiaId: tecnologia.id
    };

    this.candidatosService.atualizarTecnologia(novaTecnologia)
      .subscribe(result => { });
  }

  private candidatoPossuiTecnologia(candidato, tecnologiaId) {
    if (!candidato.tecnologias || candidato.tecnologias.length === 0) {
      return false;
    }

    const idTecnologiasCandidato = candidato.tecnologias.map(m => m.tecnologiaId);
    return idTecnologiasCandidato.indexOf(tecnologiaId) !== -1;
  }

}
