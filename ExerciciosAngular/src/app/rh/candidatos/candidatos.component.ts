import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { CandidatosService } from './candidatos.service';
import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';
import { RemoveDialogComponent } from '../../geral/dialogs/remove-dialog/remove-dialog.component';

@Component({
  selector: 'app-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.css']
})
export class CandidatosComponent implements OnInit {
  displayedColumns = ['actions', 'candidato'];
  dataSource;

  constructor(
    private candidatosService: CandidatosService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.atualizarLista();
  }

  atualizarLista() {
    this.candidatosService.getEntities().subscribe(candidatos => {
      this.dataSource = candidatos;
    });
  }

  openAddDialog(candidato) {
    this.candidatosService.novoCandidato(candidato, () => this.atualizarLista());
  }

  openRemoveDialog(candidato) {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      data: {
        title: 'Confirma a deleção do Candidato?',
        body: 'O candidato *' + candidato.nome + '* será removido',
        descricao: candidato.nome,
        id: candidato.id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      this.candidatosService.removeEntity(result.id).subscribe(_ => {
        this.atualizarLista();
      });
    });
  }

}
