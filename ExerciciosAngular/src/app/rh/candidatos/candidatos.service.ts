import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';

import { CrudServiceBase } from '../../geral/crud-service-base.service';
import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';

@Injectable()
export class CandidatosService extends CrudServiceBase {
  protected apiBasePath = 'candidatos';

  constructor(
    http: HttpClient,
    public dialog: MatDialog
  ) {
    super(http);
  }

  atualizarTecnologia(tecnologia) {
    return this.http.post<any>(this.endpoint + '/atualizarTecnologia', tecnologia);
  }

  novoCandidato(candidato, callback?) {
    function runCallback(candidatoCriado) {
      if (callback) {
        callback(candidatoCriado);
      }
    }

    const data: any = {
      title: 'Informe o nome do candidato'
    };
    if (candidato) {
      data.id = candidato.id;
      data.descricao = candidato.nome;
    }
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      result.nome = result.descricao;

      if (result.id) {
        this.updateEntity(result).subscribe(savedEntity => runCallback(savedEntity));
      } else {
        this.addEntity(result).subscribe(savedEntity => runCallback(savedEntity));
      }
    });
  }
}
