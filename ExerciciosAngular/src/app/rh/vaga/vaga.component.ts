import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

import { VagaService } from './vaga.service';
import { ActivatedRoute } from '@angular/router';
import { AddCandidatoDialogComponent } from './add-candidato-dialog/add-candidato-dialog.component';
import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';

@Component({
  selector: 'app-vaga',
  templateUrl: './vaga.component.html',
  styleUrls: ['./vaga.component.css']
})
export class VagaComponent implements OnInit {
  displayedColumns = ['actions', 'tecnologia', 'nota'];
  displayedColumnsCandidatos = ['actions', 'candidato', 'nota'];

  tecnologias: any[];
  candidatos: any[];

  vaga: any = {};
  vagaId;

  constructor(
    private route: ActivatedRoute,
    private vagaService: VagaService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.route.params.subscribe(params =>
      this.vagaId = params['id']
    );
  }

  ngOnInit() {
    this.atualizarLista();
  }

  atualizarLista() {
    this.vagaService.obterInformacoesVaga(this.vagaId)
      .subscribe(informacoes => {
        this.vaga = informacoes.vaga;
        this.tecnologias = informacoes.tecnologias;
        this.candidatos = informacoes.candidatos;
      });
  }

  updateTecnologia(tecnologia) {
    this.vagaService.updateTecnologia(this.vaga, tecnologia)
      .subscribe(_ => {
      });
  }

  openAddCandidatoDialog() {
    const data: any = {
      id: this.vaga.id
    };
    const dialogRef = this.dialog.open(AddCandidatoDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      if (result.id) {
        this.updateCandidato(result.candidato);
      }
    });
  }

  openAddTecnologiaDialog(event, tecnologia) {
    event.preventDefault();
    event.stopPropagation();

    if (tecnologia.possuiTecnologia) {
      tecnologia.possuiTecnologia = false;
      tecnologia.nota = null;
      this.updateTecnologia(tecnologia);
      return;
    }

    const data: any = {
      title: 'Informe a nota da tecnologia *' + tecnologia.descricao + '* para a vaga',
      id: this.vaga.id
    };
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      const nota = parseInt(result.descricao, 10);

      if (!nota || nota < 0) {
        const snackBarRef = this.snackBar.open('Nota inválida informada: ' + result.descricao, 'Tentar novamente', {

        });
        snackBarRef.onAction().subscribe(() => {
          this.openAddTecnologiaDialog(event, tecnologia);
        });
        return;
      }

      tecnologia.possuiTecnologia = true;
      tecnologia.nota = nota;
      this.updateTecnologia(tecnologia);
    });
  }

  updateCandidato(candidato) {
    this.vagaService.updateCandidato(this.vaga, candidato).subscribe(_ => {
      this.atualizarLista();
    });
  }
}
