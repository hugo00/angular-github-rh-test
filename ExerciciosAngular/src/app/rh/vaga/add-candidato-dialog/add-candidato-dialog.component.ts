import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CandidatosService } from '../../candidatos/candidatos.service';
import { VagaService } from '../vaga.service';
import { VagasService } from '../../vagas/vagas.service';

@Component({
  selector: 'app-add-candidato-dialog',
  templateUrl: './add-candidato-dialog.component.html',
  styleUrls: ['./add-candidato-dialog.component.css']
})
export class AddCandidatoDialogComponent implements OnInit {
  candidatos: any[];

  constructor(
    public dialogRef: MatDialogRef<AddCandidatoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private vagasService: VagasService
  ) { }

  ngOnInit(): void {
    this.vagasService.candidatosPossiveisParaVaga(this.data.id)
      .subscribe(candidatos => this.candidatos = candidatos);
  }

  close(result?): void {
    this.dialogRef.close(result);
  }

  possuiCandidatos() {
    return this.candidatos && this.candidatos.length > 0;
  }

}
