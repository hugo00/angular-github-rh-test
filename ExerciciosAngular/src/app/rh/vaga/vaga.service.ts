import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';

import { TecnologiasService } from '../tecnologias/tecnologias.service';
import { VagasService } from '../vagas/vagas.service';

@Injectable()
export class VagaService {

  constructor(
    private vagasService: VagasService,
    private tecnologiasService: TecnologiasService
  ) { }

  obterInformacoesVaga(id) {
    return Observable.combineLatest(
      this.vagasService.getEntity(id),
      this.tecnologiasService.getEntities(),
    ).map(informacoes => {
      const [vaga, tecnologias] = informacoes;
      if (vaga.tecnologias && vaga.tecnologias.length > 0) {
        tecnologias.forEach(tecnologia => {
          this.atualizarInformacoesDasTecnologias(vaga, tecnologia);
        });
      }
      return { vaga, tecnologias, candidatos: this.obterCandidatosOrdenadosPorNota(vaga) };
    });
  }

  private obterCandidatosOrdenadosPorNota(vaga) {
    const notasPorTecnologia = {};
    vaga.tecnologias.forEach(t => {
      notasPorTecnologia[t.tecnologiaId] = t.nota;
    });
    const tecnologiasDaVaga = vaga.tecnologias.map(t => t.tecnologiaId);

    const resultado = vaga.candidatos.map(c => {
      const tecnologiasDoCandidatoRelacionadas: any[] = c.candidato.tecnologias
        .filter(t => tecnologiasDaVaga.indexOf(t.tecnologiaId) !== -1);
      const notaCandidato = tecnologiasDoCandidatoRelacionadas.reduce(
        (valorAtual, t) => notasPorTecnologia[t.tecnologiaId] + valorAtual,
        0
      );
      const candidatoComNota = { ...c.candidato, nota: notaCandidato };
      return candidatoComNota;
    });

    return resultado.sort((a, b) => {
      if (a.nota < b.nota) {
        return 1;
      }
      if (a.nota > b.nota) {
        return -1;
      }
      return 0;
    });
  }

  updateTecnologia(vaga, tecnologia) {
    const novaTecnologia = {
      vagaId: vaga.id,
      tecnologiaId: tecnologia.id,
      nota: tecnologia.nota || 0
    };

    return this.vagasService.atualizarTecnologia(novaTecnologia);
  }

  updateCandidato(vaga, candidato) {
    return this.updateCandidatoByIds(vaga.id, candidato.id);
  }
  updateCandidatoByIds(vagaId, candidatoId) {
    const novoCandidato = {
      vagaId: vagaId,
      candidatoId: candidatoId
    };

    return this.vagasService.atualizarCandidato(novoCandidato);
  }

  private atualizarInformacoesDasTecnologias(vaga, tecnologia) {
    if (!vaga.tecnologias || vaga.tecnologias.length === 0) {
      return;
    }

    vaga.tecnologias.forEach(t => {
      if (t.tecnologiaId === tecnologia.id) {
        tecnologia.possuiTecnologia = true;
        tecnologia.nota = t.nota;
        return;
      }
    });
  }

}
