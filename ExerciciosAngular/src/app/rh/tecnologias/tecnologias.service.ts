import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigurationManager } from '../configuration';
import { CrudServiceBase } from '../../geral/crud-service-base.service';

@Injectable()
export class TecnologiasService extends CrudServiceBase {
  protected apiBasePath = 'tecnologias';

  constructor(http: HttpClient) {
    super(http);
   }
}
