import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { TecnologiasService } from './tecnologias.service';
import { AddDialogComponent } from '../../geral/dialogs/add-dialog/add-dialog.component';
import { RemoveDialogComponent } from '../../geral/dialogs/remove-dialog/remove-dialog.component';

@Component({
  selector: 'app-tecnologias',
  templateUrl: './tecnologias.component.html',
  styleUrls: ['./tecnologias.component.css']
})
export class TecnologiasComponent implements OnInit {
  displayedColumns = ['actions', 'tecnologia'];
  dataSource;

  constructor(
    private tecnologiasService: TecnologiasService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.atualizarLista();
  }

  atualizarLista() {
    this.tecnologiasService.getEntities().subscribe(tecnologias => {
      this.dataSource = tecnologias;
    });
  }

  openAddDialog(tecnologia) {
    const data: any = {
      title: 'Informe a descrição para a tecnologia'
    };
    if (tecnologia) {
      data.id = tecnologia.id;
      data.descricao = tecnologia.descricao;
    }
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      if (result.id) {
        this.tecnologiasService.updateEntity(result).subscribe(_ => {
          this.atualizarLista();
        });
      } else {
        this.tecnologiasService.addEntity(result).subscribe(_ => {
          this.atualizarLista();
        });
      }
    });
  }

  openRemoveDialog(tecnologia) {
    const dialogRef = this.dialog.open(RemoveDialogComponent, {
      data: {
        title: 'Confirma a deleção da Tecnologia?',
        body: 'A tecnologia *' + tecnologia.descricao + '* será removida',
        descricao: tecnologia.descricao,
        id: tecnologia.id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) { return; }

      this.tecnologiasService.removeEntity(result.id).subscribe(_ => {
        this.atualizarLista();
      });
    });
  }

}
