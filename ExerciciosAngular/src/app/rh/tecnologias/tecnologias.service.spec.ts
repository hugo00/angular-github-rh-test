import { TestBed, inject } from '@angular/core/testing';

import { TecnologiasService } from './tecnologias.service';

describe('TecnologiasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TecnologiasService]
    });
  });

  it('should be created', inject([TecnologiasService], (service: TecnologiasService) => {
    expect(service).toBeTruthy();
  }));
});
