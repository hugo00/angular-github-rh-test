export class ConfigurationManager {
    public static apiUrl(path: string): string {
        if (path && path[0] === '/') { path =  path.substring(1, path.length); }
        return 'http://localhost:57949/api/' + path;
    }
}
