import { HttpClient } from '@angular/common/http';

import { ConfigurationManager } from '../rh/configuration';

export abstract class CrudServiceBase {
    protected abstract get apiBasePath();

    protected get endpoint() { return ConfigurationManager.apiUrl(this.apiBasePath); }

    constructor(
        protected http: HttpClient
    ) { }

    getEntity(id) {
        return this.http.get<any>(this.endpoint + '/' + id);
    }

    getEntities() {
        return this.http.get<any>(this.endpoint);
    }

    addEntity(entity) {
        return this.http.post<any>(this.endpoint, entity);
    }

    removeEntity(id) {
        return this.http.delete<any>(this.endpoint + '/' + id);
    }

    updateEntity(entity) {
        return this.http.post<any>(this.endpoint, entity);
    }

}
