import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  MatInputModule,
  MatButtonModule,
  MatSelectModule,
  MatIconModule,
  MatTableModule,
  MatToolbarModule,
  MatCardModule,
  MatGridListModule,
  MatSidenavModule,
  MatListModule,
  MatDialogModule,
  MatCheckboxModule,
  MatTabsModule,
  MatSnackBarModule,
  MatStepperModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './geral/page-not-found/page-not-found.component';
import { APP_ROUTING } from './app.routing';
import { UsuariosComponent } from './github/usuarios/usuarios.component';
import { UsuariosService } from './github/usuarios/usuarios.service';
import { UsuarioComponent } from './github/usuario/usuario.component';
import { MainComponent } from './geral/main/main.component';
import { TecnologiasComponent } from './rh/tecnologias/tecnologias.component';
import { GithubComponent } from './github/github/github.component';
import { RhComponent } from './rh/rh/rh.component';
import { RhLayoutComponent } from './rh/rh-layout/rh-layout.component';
import { CandidatosComponent } from './rh/candidatos/candidatos.component';
import { VagasComponent } from './rh/vagas/vagas.component';
import { TecnologiasService } from './rh/tecnologias/tecnologias.service';
import { CandidatosService } from './rh/candidatos/candidatos.service';
import { AddDialogComponent } from './geral/dialogs/add-dialog/add-dialog.component';
import { RemoveDialogComponent } from './geral/dialogs/remove-dialog/remove-dialog.component';
import { CandidatoComponent } from './rh/candidato/candidato.component';
import { CandidatoService } from './rh/candidato/candidato.service';
import { VagasService } from './rh/vagas/vagas.service';
import { VagaComponent } from './rh/vaga/vaga.component';
import { VagaService } from './rh/vaga/vaga.service';
import { AddCandidatoDialogComponent } from './rh/vaga/add-candidato-dialog/add-candidato-dialog.component';
import { EntrevistaComponent } from './rh/workflows/entrevista/entrevista.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    UsuariosComponent,
    UsuarioComponent,
    MainComponent,
    TecnologiasComponent,
    AddDialogComponent,
    GithubComponent,
    RhComponent,
    RhLayoutComponent,
    CandidatosComponent,
    VagasComponent,
    RemoveDialogComponent,
    CandidatoComponent,
    VagaComponent,
    AddCandidatoDialogComponent,
    EntrevistaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
    MatDialogModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSnackBarModule,
    MatStepperModule,
    APP_ROUTING
  ],
  providers: [
    UsuariosService,
    TecnologiasService,
    CandidatoService,
    CandidatosService,
    VagasService,
    VagaService
  ],
  entryComponents: [
    AddDialogComponent,
    RemoveDialogComponent,
    AddCandidatoDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
