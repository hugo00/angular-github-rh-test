import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuariosService } from '../usuarios/usuarios.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  login;
  usuario = {};

  displayedColumns = ['id', 'name', 'html_url'];
  repos;

  constructor(
    private route: ActivatedRoute,
    private usuariosService: UsuariosService
  ) {
    this.route.params.subscribe(params => {
      this.login = params['login'];
    });
  }

  ngOnInit() {
    this.usuariosService.getUser(this.login).subscribe(usuario => {
      this.usuario = usuario;
    });
    this.usuariosService.getRepos(this.login).subscribe(repos => {
      this.repos = repos;
    });
  }

}
