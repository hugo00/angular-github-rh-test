import { Component, OnInit } from '@angular/core';

import { UsuariosService } from './usuarios.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  displayedColumns = ['id', 'login'];
  dataSource;

  constructor(private usuariosService: UsuariosService) { }

  ngOnInit() {
    this.usuariosService.getUsers().subscribe(usuarios => {
      this.dataSource = usuarios;
    });
  }

}
