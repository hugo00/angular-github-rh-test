import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UsuariosService {
  private endpointUsers = 'https://api.github.com/users';

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<any>(this.endpointUsers);
  }

  getUser(username: string) {
    return this.http.get<any>(this.endpointUsers + '/' + username);
  }

  getRepos(username: string) {
    return this.http.get<any>(this.endpointUsers + '/' + username + '/repos');
  }

}
